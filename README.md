## Description
Java - Spring Boot CRUD without database


```bash
mvn compile
```
To run the application 
```bash
mvn spring-boot:run
```

Default port : 8083
To change the port : [FileToOpen](./src/main/resources/application.properties)



To deploy on Tomcat
```bash
mvn clear package
```

Then debug : /target/demo-sin-bd.war  on tomcat server